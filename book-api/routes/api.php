<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'API\UserController@register');
Route::post('login', 'API\UserController@login');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('logout', 'API\UserController@logout');
    Route::get('user', 'API\UserController@me');
    Route::apiResource('books', 'API\BookController');
    Route::post('processbook', 'API\BookController@processbook');
});

