<?php

use Illuminate\Database\Seeder;
use App\Book;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void 
     */
    public function run()
    {
        $filePath = base_path("resources/books.csv"); 
        foreach (array_slice(glob($filePath),0,2) as $file) {
            $data = array_map('str_getcsv', file($file));

                foreach($data as $row) {
                    $publication_year = $row[8] != '' ? $row[8] : null;
                    $total_ratings = is_numeric($row[13]) || $row[13] != '' ? $row[13] : null ;
                    $average_ratings = is_numeric($row[12]) || $row[12] != '' ? $row[12] : null ;
                    $title = $row[10] != '' ? $row[10] : null ;

                    $user = Book::create([
                        'title' => $title, 
                        'original_title' => $row[9],
                        'publication_year' => $publication_year,
                        'isbn' => $row[5],
                        'language_code' => $row[11],
                        'image' => $row[21],
                        'thumbnail' => $row[22],
                        'average_ratings' => $average_ratings,
                        'total_ratings' => $total_ratings,
                    ]);
                }

        }
    }
}
