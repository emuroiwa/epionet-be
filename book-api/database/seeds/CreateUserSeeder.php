<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Epionet User', 
            'email' => 'user@epionet.co.za',
            'password' => bcrypt('12345678')
        ]);
    }
}
