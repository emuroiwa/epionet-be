<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BookCollection;
use App\Book;
use App\User;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return new BookCollection(Book::all());

    }

     /**
     * process book given the  specified process_type .
     *
     * @param  \Illuminate\Http\Request  $request 
     * @return \Illuminate\Http\Response
     */
    public function processbook(Request $request)
    {
        $this->validate($request,[
            'book_id' => 'required|max:50',
            'user_id' => 'required|max:50',
            'process_type' => 'required|max:20',
        ]);
        //further validation
        if (!User::find($request['user_id'])) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user with id ' . $request['user_id'] 
                . ' cannot be found'
            ], 400);
        }
        if (!Book::find($request['book_id'])) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, book with id ' . $request['book_id'] 
                . ' cannot be found'
            ], 400);
        }

        $book = resolve('App\MyLibraryServices\MyLibrary');
        if ($request['process_type'] == "checkout") {
            return $book->checkOutBook($request['user_id'], $request['book_id']);

        } elseif ($request['process_type'] == "return") {
            return $book->returnBook($request['user_id'], $request['book_id']);

        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, book with process_type ' 
                . $request['process_type'] . ' cannot be found'
            ], 400);
        }
    }
    
}
