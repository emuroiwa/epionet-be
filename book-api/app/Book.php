<?php

namespace App;

use App\User;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * DB relationship User to Books.
     *
     * @return 
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
