<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\MyLibraryServices\MyLibrary;

class LibraryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        app()->singleton(MyLibrary::class, function(){  
            return new MyLibrary;
        });
  
    }
}
