<?php

namespace App\MyLibraryServices;
use Carbon\Carbon;
use App\Book;
use App\User;

/**
* Library service class to process checking out and return of books
*/
class MyLibrary 
{
    /**
     * processes checking out of book.
     *
     * @param  int $user
     * @param  int $bookcheckOutBook
     * @return \Illuminate\Http\Response
     */
    public function checkOutBook($user, $book)
    {
        $current = Carbon::now();
        //this can be parameterised
        $dueAt = $current->addDays(7);

        $userData = User::find($user);
        $bookData = Book::find($book);

        $userData->books()->attach($bookData, [
                                'due_at'=> $dueAt, 
                                'returned_at'=>null
                                ]);

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Processes return of book
     *
     * @param  int $user
     * @param  int $book
     * @return \Illuminate\Http\Response
     */
    public function returnBook($user, $book)
    {
               
        $userData = User::find($user);
        $bookData = Book::find($book);
        
        $userData->books()->attach($bookData, [
                                'due_at'=> null,
                                'returned_at'=>now()
                                ]);

        return response()->json([
            'success' => true
        ]);
    }
}