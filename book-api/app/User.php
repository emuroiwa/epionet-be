<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Book;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * getJWTIdentifier.
     * gets identifier 
     * @return 
     */
    public function getJWTIdentifier()
    {
      return $this->getKey();
    }

    /**
     * getJWTCustomClaims.
     *
     * @return 
     */
    public function getJWTCustomClaims()
    {
      return [];
    }
        
    /**
     * DB relationship User to Books.
     *
     * @return 
     */
    public function books()
    {
      return $this->belongsToMany(Book::class)
            ->withTimestamps()
            ->withPivot('due_at', 'returned_at');
    }

}
